﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UBT.Eshop.Areas.Admin.Data;
using UBT.Eshop.Areas.Admin.ViewModels.Products;

namespace UBT.Eshop.Areas.Eshop.Controllers
{
    [Area("Eshop")]
    public class ProductsController : Controller
    {
        private ProductsData products;

        public ProductsController(ProductsData products)
        {
            this.products = products;
        }

        public IActionResult Index()
        {
            ProductsViewModel pvm = products.GetData();
            return View(pvm);
        }

        // GET: Products/Details/5
        public ActionResult Details(int id)
        {
            var product = products.GetProduct(id);

            return View(product);
        }
    }
}