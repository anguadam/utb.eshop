﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using UBT.Eshop.Areas.Admin.ViewModels.Products;

namespace UBT.Eshop.Areas.Admin.Data
{
    public class ProductsData
    {      
              
        private ProductsViewModel productsViewModel = new ProductsViewModel();

        public ProductsData()
        {
            productsViewModel.Products = new Collection<ProductViewModel>
            {
                new ProductViewModel
                {
                    Id = 1,
                    DateCreated = DateTime.Now,
                    ImageUrl = "./image.png",
                    Name = "Produkt 1",
                    Price = 100800
                },

                new ProductViewModel
                {
                    Id = 2,
                    DateCreated = DateTime.Now,
                    ImageUrl = "./image.png",
                    Name = "Produkt 2",
                    Price = 508080
                },
            };
        }

        public ProductsViewModel GetData()
        {
            return productsViewModel;

        }

        public void PutData(ProductViewModel model)
        {
            productsViewModel.Products.Add(model);         
        }


        public void Edit(ProductsViewModel model, int id)
        {
            var product = productsViewModel.Products.FirstOrDefault(x => x.Id == id);            
        }
      
        public ProductViewModel GetProduct(int id)
        {
            var product = productsViewModel.Products.FirstOrDefault(x => x.Id == id);
           
            return product;

        }

        public void Delete(int id)
        {
            var product = productsViewModel.Products.FirstOrDefault(x => x.Id == id);
            productsViewModel.Products.Remove(product);
        }

    }
}
