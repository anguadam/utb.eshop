﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UBT.Eshop.Areas.Admin.Data;
using UBT.Eshop.Areas.Admin.ViewModels.Products;

namespace UBT.Eshop.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ProductsController : Controller
    {
        private ProductsData products;

        public ProductsController(ProductsData products)
        {
            this.products = products;
        }

        // GET: Products
        public ActionResult Index()
        {
            //return trough indexviewmodel

            ProductsViewModel pvm = products.GetData();

            return View(pvm);
        }

        // GET: Products/Details/5
        public ActionResult Details(int id)
        {
            var product = products.GetProduct(id);

            return View(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {

            return View();
        }

        // POST: Products/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductViewModel model)
        {
            try
            {

                products.PutData(model);
                return RedirectToAction(nameof(Index));

            }
            catch
            {
                return View();
            }
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int id)
        {


            return View();
        }

        // POST: Products/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        
       // GET: Products/Delete/5
        public ActionResult Delete(int id)
        {

            products.Delete(id);

            return RedirectToAction(nameof(Index));
        }

        /*
//         POST: Products/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        */
        
    }
}