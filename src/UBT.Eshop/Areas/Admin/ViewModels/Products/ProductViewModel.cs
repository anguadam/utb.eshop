﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UBT.Eshop.Areas.Admin.ViewModels.Products
{
    public class ProductViewModel
    {

        public int Id { get; set; }

        public DateTime DateCreated { get; set; }

        public string Name { get; set; }

        public int Price { get; set; }

        public string ImageUrl { get; set; }

    }
}
